# Cat-Dog-Classification

A classification project to classify Cat and Dog using Keras neural network API, written in Python running on top of Tensorflow. 


Instruction for running the project:
1. Download the dataset from https://www.kaggle.com/c/dogs-vs-cats-redux-kernels-edition/data and add to your project folder.
2. Run train_model.py to create and train the model.
3. Run use_model.py to classify the new data using the trained model we previously created. 
