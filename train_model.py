import cv2

from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

from keras.preprocessing.image import ImageDataGenerator

from matplotlib import pyplot as plt

#create a model
model = Sequential()

#add necessary layers to the model
model.add(Conv2D(32,(3,3),input_shape=(50,50,3),activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(32,(3,3),activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())

model.add(Dense(units=128, activation='relu'))
model.add(Dense(units=1, activation='sigmoid'))

#compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

train_data = ImageDataGenerator(rescale=1./255, shear_range=0.2, zoom_range=0.2, horizontal_flip=True)
test_data = ImageDataGenerator(rescale=1./255)

#prepare the training data
train_generator = train_data.flow_from_directory('./data/train',
                                                 target_size=(50,50),
                                                 batch_size=32,
                                                 class_mode='binary')

#prepare the validation data
validation_generator = test_data.flow_from_directory('./data/test',
                                                     target_size=(50,50),
                                                     batch_size=32,
                                                     class_mode='binary')
#acquire history for visualizing the graphs
history = model.fit_generator(train_generator,
                    steps_per_epoch=8000,
                    epochs=25,
                    validation_data=validation_generator,
                    validation_steps=2000
                    )

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

print("History")
print(history.history.keys())

#save the trained model to a json file
model_json = model.to_json()
with open("./model/model.json","w") as json_file:
    json_file.write(model_json)

#save the weights to h5 file
model.save_weights("./model/model.h5")
print("Model Saved!")

#plot the graph for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.title('validation accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['Train', 'Validation'],loc='upper left')
plt.show()


#plot the graph for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.title('loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['Train', 'Validation'],loc='upper left')
plt.show()

#'val_loss', 'val_acc', 'loss', 'acc'

#classify the given image data : 0 for Cat; 1 for Dog
img = cv2.imread("./data/cat_test.jpg")
img = cv2.resize(img,(50,50))
img = img.reshape(1,50,50,3)

print(model.predict(img))
