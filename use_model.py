import cv2
from keras.models import model_from_json

json_file = open('./model/model.json','r')
loaded_model_json = json_file.read()
json_file.close()

loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights('./model/model.h5')
print("Model Loaded from Disk")

loaded_model.compile(optimizer='adam', loss='binary_crossentropy', metrics = ['accuracy'])
# print("History")
# history = train_model.old_history
# print(history.history.keys())


img = cv2.imread("./data/dog_test.jpg")
img = cv2.resize(img,(50,50))
img = img.reshape(1,50,50,3)
print(loaded_model.predict(img))